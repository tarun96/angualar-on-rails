import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: Object;
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.showUser(3).subscribe(data => {
      this.user = data
      console.log(this.user);
    }
  );
  }

}
