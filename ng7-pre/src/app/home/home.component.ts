import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private data: DataService) { }
  hovered :boolean = false;
  h1Style: boolean = false;
  users: Object;
  p: number = 1;
  
  ngOnInit() {
    this.data.getUsers().subscribe(data => {
        this.users = data
        console.log(this.users);
      }
    );
  }

  entered(){
    this.hovered = !this.hovered;
  }

  leaved(){
    this.hovered = !this.hovered;
  }
  firstClick() {
    this.data.firstClick();
  }

}
