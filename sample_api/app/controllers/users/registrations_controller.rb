class Users::RegistrationsController < Devise::RegistrationsController

  def create
    @user = User.new register_params
    if @user.save
      render json: @user
    else
      render json: @user.errors, status: :error_occurred
    end
  end

  def register_params
    params.require(:registration).permit(:first_name, :last_name, :email, :password)
  end

end