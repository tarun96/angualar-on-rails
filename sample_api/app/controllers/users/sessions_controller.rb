class Users::SessionsController < Devise::SessionsController

  def create
    @user = User.find_by(email: params[:email]) 
    if @user.present?
      if @user.valid_password? params[:password]
        render json: { user: @user , message: 'Found' }
      else
        render json: { message: 'Invalid email password combination' }
      end  
      else
      render json: { message: 'User not found' }
    end
  end

end