import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {AuthenticationService} from '../authentication/authentication.service'
import {Observable} from 'rxjs/Rx';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authservice: AuthenticationService,private flash: FlashMessagesService) { }
  success: boolean = false;
  failure: boolean = false;
  user = new User;

  ngOnInit() {
  }

  login(user: User){
    this.authservice.login(user).subscribe(
      data => {
        console.log(data.message);
        if(data.message == "Found"){
          this.flash.show(data.message, { cssClass: 'alert-success', timeout: 1000});
          this.success = true;
          return true
        }
        else{
          this.flash.show(data.message, { cssClass: 'alert-success', timeout: 1000 });
          return false;
        }
      }
    )
  }

}
