import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http'; 
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import {User} from '../user'

@Injectable()
export class AuthenticationService{
  private baseUrl = 'http://localhost:3000/';

  constructor(private http: HttpClient){}

  public register(user: User){
    var url = this.baseUrl + "users"
    return this.http.post(url,user);
  }

  public login(user: User){
    var url = this.baseUrl + "users/sign_in"
    return this.http.post(url,user);
  }
    
}

