import { NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { PostListComponent } from './post/post-list.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PostShowComponent } from './post/post-show.component';
import { PostNewComponent } from './post/post-new.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'home', component: HomepageComponent},
    {path: 'posts', component: PostListComponent},
    {path: 'posts/:id', component: PostShowComponent},
    {path: 'post/new', component: PostNewComponent},
    {path: 'signup', component: RegistrationComponent},
    {path: 'login', component: LoginComponent},
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
    })

  export class AppRoutingModule { }