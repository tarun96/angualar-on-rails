import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {AuthenticationService} from '../authentication/authentication.service'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private authservice: AuthenticationService) { }
  success: boolean = false;
  failure: boolean = false;
  user = new User;

  ngOnInit() {
  }

  register(user: User){
    this.authservice.register(user).subscribe(
      data => {
        this.success = true;
        return true
        
      },
      error => {
        this.failure = true;
        // console.log(Observable.throw(error));
      }
    )
  }

}
