import { Component, OnInit } from '@angular/core';
import {Post} from './post'
import {PostService} from './post.service'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post.component.css']
})

export class PostNewComponent {
  post = new Post;
  submitted: boolean = false;

  constructor(private postService: PostService){}

  create(post: Post){
    this.submitted = true;
    this.postService.create(post).subscribe(
      data => {return true},
      error => {
        console.log("Error in creating a post");
        return Observable.throw(error);
      }
    )
  }

}