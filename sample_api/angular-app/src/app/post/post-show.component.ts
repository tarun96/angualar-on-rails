import { Component, OnInit , Input } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Post} from './post'
import {PostService} from './post.service'
import 'rxjs/add/observable/timer';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Http} from '@angular/http';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'post-show',
  templateUrl: './post-show.component.html',
  styleUrls: ['./post.component.css']
})
export class PostShowComponent implements OnInit {
  constructor(private postService: PostService, private route: ActivatedRoute, private http: Http, private router: Router ){ }
  id: number;
  routeId: any; 
  editClicked: boolean = false;
  @Input() post: Post;

  ngOnInit() {
    this.routeId = this.route.params.subscribe(params => {
      this.id = +params['id'];
    })
    let postRequest = this.route.params.flatMap((params: Params) => this.postService.show(+params['id']));
    // postRequest.subscribe(response => this.post = response.json())
    this.postService.show(this.id).subscribe((data : Post)=>{
      console.log(data);
      this.post = data;
    });
  }

  toggleform(){
    this.editClicked = !this.editClicked;
  }

  delete(id: number){
    this.postService.delete(id).subscribe((data : Post)=>{
    this.goback();
    });
  }

  update(post: Post,id: number){
    this.postService.update(post,id).subscribe((data : Post)=>{
      this.goback();
    });
  }

  goback(): void{
    let postLink = '/posts'
    this.router.navigate([postLink]);
  }


}
