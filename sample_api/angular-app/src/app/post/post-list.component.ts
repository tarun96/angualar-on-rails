import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Post} from './post'
import {PostService} from './post.service'
import 'rxjs/add/observable/timer';
import {Router} from '@angular/router';

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post.component.css']
})
export class PostListComponent implements OnInit {
  posts: Post[];
  // public columns = ['id','title'];
  // public rows : Array<Post>; 
  // posts = this.rows;
  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    // let timer = Observable.timer(0,5000);
    // timer.subscribe(() => this.getPosts());
    this.postService.get().subscribe((data : Post[])=>{
      console.log(data);
      this.posts = data;
    });


  }

  gotoshow(post: Post): void{
    let postLink = '/posts' + "/" + post.id;
    console.log(postLink);
    this.router.navigate([postLink]);
  }


}
