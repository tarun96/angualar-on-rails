import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http'; 
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import {Post} from './post'

@Injectable()
export class PostService{
    private postsUrl = 'http://localhost:3000/posts';

    constructor(private http: HttpClient){}

    public get(){
      return this.http.get(this.postsUrl);
    }

    public show(id: number){
      return this.http.get(this.postsUrl + "/" + id)
    }

    public create(post: any){
      var endpoint = this.postsUrl
      return this.http.post(endpoint,post);
    }

    public update(post: any,id:number){
      var endpoint = this.postsUrl + "/" + id
      return this.http.put(endpoint,post)
    }

    public delete(id: number){
      var endpoint = this.postsUrl + "/" + id
      return this.http.delete(endpoint)
    }
}

